package br.com.simuladorcartao.cliente.cliente.controller;

import br.com.simuladorcartao.cliente.cliente.controller.request.CadastroClienteRequest;
import br.com.simuladorcartao.cliente.cliente.controller.response.ClienteResponse;
import br.com.simuladorcartao.cliente.cliente.dataprovider.model.Cliente;
import br.com.simuladorcartao.cliente.cliente.mapper.ClienteMapper;
import br.com.simuladorcartao.cliente.cliente.security.Usuario;
import br.com.simuladorcartao.cliente.cliente.service.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.filter.CharacterEncodingFilter;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/cliente")
public class ClienteController {

    @Autowired
    private ClienteService clienteService;

    @Autowired
    private ClienteMapper clienteMapper;

//    @Value("${server.port}")
//    private int porta;

    /**
     * Cadastrar Cliente
     * @param cadastroClienteRequest
     * @return clienteResponse
     */
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ClienteResponse cadastrarCliente(@Valid @RequestBody CadastroClienteRequest cadastroClienteRequest) {
        Cliente cliente = clienteService.cadastrarCliente(clienteMapper.converterParaCliente(cadastroClienteRequest));
        return clienteMapper.converterParaCadastroClienteResponse(cliente);
    }

    /**
     * Consultar Cliente por Id
     * @param id
     * @return cliente
     */
    @GetMapping("/{id}")
    public ClienteResponse consultarClientePorId(@PathVariable(name = "id") long id) {
        Cliente cliente = clienteService.consultarClientePorId(id);
        //System.out.println("Pesquisa por cliente id " + id + " na porta " + porta + " em " + System.currentTimeMillis());
        return clienteMapper.converterParaCadastroClienteResponse(cliente);
    }

}
