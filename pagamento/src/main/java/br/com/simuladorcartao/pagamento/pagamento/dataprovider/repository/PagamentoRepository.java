package br.com.simuladorcartao.pagamento.pagamento.dataprovider.repository;

import br.com.simuladorcartao.pagamento.pagamento.dataprovider.model.Pagamento;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PagamentoRepository extends JpaRepository<Pagamento, Long> {

    List<Pagamento> findByIdCartao(long id);

}
