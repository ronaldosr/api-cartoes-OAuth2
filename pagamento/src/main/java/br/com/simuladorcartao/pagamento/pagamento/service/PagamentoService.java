package br.com.simuladorcartao.pagamento.pagamento.service;

import br.com.simuladorcartao.pagamento.pagamento.dataprovider.model.Pagamento;
import br.com.simuladorcartao.pagamento.pagamento.dataprovider.repository.PagamentoRepository;
import br.com.simuladorcartao.pagamento.pagamento.exception.CartaoInativoException;
import br.com.simuladorcartao.pagamento.pagamento.gateway.CartaoGateway;
import br.com.simuladorcartao.pagamento.pagamento.gateway.cartao.exception.CartaoNotFoundException;
import br.com.simuladorcartao.pagamento.pagamento.gateway.cartao.response.CartaoResponse;
import feign.FeignException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoGateway cartaoGateway;

    /**
     *
     * @param cartao_id
     * @param pagamento
     * @return
     */
    public Pagamento cadastrarPagamento(long cartao_id, Pagamento pagamento) {
        try {
            CartaoResponse cartaoResponse = cartaoGateway.consultarCartaoPorId(cartao_id);
            if (!cartaoResponse.isAtivo()) {
                throw new CartaoInativoException("O cartão não está ativo");
            }
            pagamento.setIdCartao(cartaoResponse.getId());
            return pagamentoRepository.save(pagamento);
        } catch (CartaoNotFoundException e) {
            throw new CartaoNotFoundException(e);
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao cadastrar o pagamento: ", e);
        }
    }

    /**
     * Consultar Pagamento por Id Cartão
     * @param id_cartao
     * @return
     */
    public List<Pagamento> consultarPagamentosPorId(long id_cartao) {
        try {
            List<Pagamento> listaPagamento = pagamentoRepository.findByIdCartao(id_cartao);
            return listaPagamento;
        } catch (DataAccessException e) {
            throw new RuntimeException("Erro ao consultar pagamento: ", e);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro no serviço de pagamento: ", e);
        }
    }

    /**
     * Efetuar Pagamento por Id Cartão
     * @param id_cartao
     */
    public void efetuarPagamento(long id_cartao) {
        try {
            List<Pagamento> pagamentos = pagamentoRepository.findByIdCartao(id_cartao);
            pagamentoRepository.deleteAll(pagamentos);
        } catch (DataAccessException e ) {
            throw new RuntimeException("Erro ao consultar pagamento: " + e);
        } catch (RuntimeException e) {
            throw new RuntimeException("Erro no serviço de pagamento: " , e);
        }
    }
}
