package br.com.simuladorcartao.pagamento.pagamento.mapper;

import br.com.simuladorcartao.pagamento.pagamento.controller.request.CadastroPagamentoRequest;
import br.com.simuladorcartao.pagamento.pagamento.controller.response.PagamentoResponse;
import br.com.simuladorcartao.pagamento.pagamento.dataprovider.model.Pagamento;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PagamentoMapper {
    public Pagamento converterParaPagamento(CadastroPagamentoRequest cadastroPagamentoRequest) {
        Pagamento pagamento = new Pagamento();
        pagamento.setDescricao(cadastroPagamentoRequest.getDescricao());
        pagamento.setValor(cadastroPagamentoRequest.getValor());
        return pagamento;
    }

    public PagamentoResponse converterParaPagamentoResponse(Pagamento pagamento) {
        PagamentoResponse pagamentoResponse = new PagamentoResponse();
        pagamentoResponse.setId(pagamento.getId());
        pagamentoResponse.setCartao_id(pagamento.getIdCartao());
        pagamentoResponse.setDescricao(pagamento.getDescricao());
        pagamentoResponse.setValor(pagamento.getValor());
        return pagamentoResponse;
    }

    public List<PagamentoResponse> convertarParaListaPagamentoResponse(List<Pagamento> listaPagamento) {
        List<PagamentoResponse> listaPagamentoResponse = new ArrayList<>();
        for (Pagamento pagamento : listaPagamento) {
            PagamentoResponse pagamentoResponse = new PagamentoResponse();
            pagamentoResponse.setId(pagamento.getId());
            pagamentoResponse.setCartao_id(pagamento.getIdCartao());
            pagamentoResponse.setDescricao(pagamento.getDescricao());
            pagamentoResponse.setValor(pagamento.getValor());
            listaPagamentoResponse.add(pagamentoResponse);
        }
        return listaPagamentoResponse;
    }
}
