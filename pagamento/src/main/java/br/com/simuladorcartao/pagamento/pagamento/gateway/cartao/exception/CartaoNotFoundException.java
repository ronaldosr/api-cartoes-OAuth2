package br.com.simuladorcartao.pagamento.pagamento.gateway.cartao.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "Cartão não encontrado")
public class CartaoNotFoundException extends RuntimeException{

    public CartaoNotFoundException() {
    }

    public CartaoNotFoundException(Throwable cause) {
        super(cause);
    }
}
