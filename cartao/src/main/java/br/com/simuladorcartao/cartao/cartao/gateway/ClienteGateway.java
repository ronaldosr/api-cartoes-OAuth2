package br.com.simuladorcartao.cartao.cartao.gateway;

import br.com.simuladorcartao.cartao.cartao.gateway.config.GatewayEncoderConfiguration;
import br.com.simuladorcartao.cartao.cartao.gateway.cliente.response.ClienteResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "CLIENTE", configuration = GatewayEncoderConfiguration.class)
public interface ClienteGateway {

    @GetMapping("/cliente/{id}")
    ClienteResponse consultarClientePorId(@PathVariable(name = "id") long id);
}
