package br.com.simuladorcartao.cartao.cartao.gateway.config;

import br.com.simuladorcartao.cartao.cartao.gateway.cliente.exception.ClienteDecoder;
import com.netflix.discovery.converters.Auto;
import feign.RequestInterceptor;
import feign.codec.ErrorDecoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.security.oauth2.client.feign.OAuth2FeignRequestInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.token.grant.client.ClientCredentialsResourceDetails;

public class GatewayEncoderConfiguration {

    @Bean
    public ErrorDecoder getErrorDecoder() {
        return new ClienteDecoder();
    }
}
