package br.com.simuladorcartao.fatura.fatura.gateway.cartao.request;

import javax.validation.constraints.NotNull;

public class SituacaoCartaoRequest {

    @NotNull(message = "Informe 'true' para ativar o cartão ou 'false' para desativar o cartão.")
    private boolean ativo;

    public SituacaoCartaoRequest() {
    }

    public boolean isAtivo() {
        return ativo;
    }

    public void setAtivo(boolean ativo) {
        this.ativo = ativo;
    }
}
