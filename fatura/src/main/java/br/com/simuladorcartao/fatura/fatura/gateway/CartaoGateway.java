package br.com.simuladorcartao.fatura.fatura.gateway;

import br.com.simuladorcartao.fatura.fatura.gateway.config.CartaoGatewayConfiguration;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.request.SituacaoCartaoRequest;
import br.com.simuladorcartao.fatura.fatura.gateway.cartao.response.CartaoResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import javax.validation.Valid;

@FeignClient(name = "CARTAO", configuration = CartaoGatewayConfiguration.class)
public interface CartaoGateway {

    @GetMapping("/cartao/{id}")
    CartaoResponse consultarCartaoPorId(@PathVariable(name = "id") long id);

    @PatchMapping("/cartao/{id}")
    CartaoResponse atualizarSituacaoCartao (@PathVariable(name = "id") long id,
                                            @Valid @RequestBody SituacaoCartaoRequest situacaoCartaoRequest);
}
