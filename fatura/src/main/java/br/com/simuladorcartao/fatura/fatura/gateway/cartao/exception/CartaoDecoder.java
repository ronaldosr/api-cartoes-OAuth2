package br.com.simuladorcartao.fatura.fatura.gateway.cartao.exception;

import feign.Response;
import feign.codec.ErrorDecoder;

public class CartaoDecoder implements ErrorDecoder {

    private  ErrorDecoder errorDecoder = new Default();

    @Override
    public Exception decode(String s, Response response) {
        if (response.status() == 404) {
            return new CartaoNotFoundException();
        }
        return errorDecoder.decode(s, response);
    }
}
