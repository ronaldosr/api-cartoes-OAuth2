package br.com.simuladorcartao.fatura.fatura.gateway.cliente.fallback;

import br.com.simuladorcartao.fatura.fatura.gateway.ClienteGateway;
import br.com.simuladorcartao.fatura.fatura.gateway.cliente.response.ClienteResponse;

public class ClienteFallback implements ClienteGateway {

    @Override
    public ClienteResponse consultarClientePorId(long id) {
        ClienteResponse clienteResponse = new ClienteResponse();
        clienteResponse.setId(1);
        clienteResponse.setNome("Cliente_Contingência");
        return clienteResponse;
    }
}
