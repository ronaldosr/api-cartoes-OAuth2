package br.com.simuladorcartao.fatura.fatura.controller.response;

import java.math.BigDecimal;
import java.time.LocalDate;

public class FaturaResponse {

    private long id;
    private BigDecimal valorPago;
    private LocalDate pagoEm;

    public FaturaResponse() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getValorPago() {
        return valorPago;
    }

    public void setValorPago(BigDecimal valorPago) {
        this.valorPago = valorPago;
    }

    public LocalDate getPagoEm() {
        return pagoEm;
    }

    public void setPagoEm(LocalDate pagoEm) {
        this.pagoEm = pagoEm;
    }
}
